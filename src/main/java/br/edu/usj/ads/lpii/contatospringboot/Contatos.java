package br.edu.usj.ads.lpii.contatospringboot;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Contatos extends JpaRepository<Contato, Long> {

	List<Contato> findByNome(String nome);
	
	//@Query("SELECT c FROM Contato c WHERE c.nome LIKE %?1%")
	List<Contato> findByNomeContainingIgnoreCase(String nome);

}
