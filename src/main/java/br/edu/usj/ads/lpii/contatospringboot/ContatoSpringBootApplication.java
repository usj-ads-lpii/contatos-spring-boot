package br.edu.usj.ads.lpii.contatospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatoSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatoSpringBootApplication.class, args);
	}

}
