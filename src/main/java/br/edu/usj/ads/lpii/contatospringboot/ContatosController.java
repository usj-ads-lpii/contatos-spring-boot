package br.edu.usj.ads.lpii.contatospringboot;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/contatos")
public class ContatosController {

	@Autowired
	private Contatos contatos;
	
	@GetMapping
	public ModelAndView listar() {
		List<Contato> lista = contatos.findAll();
		
		ModelAndView modelAndView = new ModelAndView("contatos");
		modelAndView.addObject("contatos", lista);

		return modelAndView;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String listarContato(@PathVariable("id") Long id, Model model) {
		Optional<Contato> contato = contatos.findById(id);
		
		if (contato.isPresent()) {
			model.addAttribute("contato", contato.get());
		}
		
		return "mostraContato";
	}
	
	@RequestMapping(value = "/nome/{nome}", method = RequestMethod.GET)
	public ModelAndView pesquisarPorNome(@PathVariable("nome") String nome, Model model) {
		List<Contato> lista = contatos.findByNomeContainingIgnoreCase(nome);
		
		if (lista.isEmpty()) {
			model.addAttribute("contato", lista);
		}
		
		ModelAndView modelAndView = new ModelAndView("contatos");
		modelAndView.addObject("contatos", lista);

		return modelAndView;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String adicionarContato(HttpServletRequest request, @RequestParam("nome") String nome, @RequestParam("email") String email) {
		Contato contato = new Contato();
		contato.setNome(nome);
		contato.setEmail(email);
		Contato novoContato = contatos.save(contato);
		return "redirect:" + request.getRequestURI() + "/" + novoContato.getId();
	}
	
}
